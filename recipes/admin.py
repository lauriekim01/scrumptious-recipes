from django.contrib import admin


# Register your models here.
from recipes.models import Recipe

admin.site.register(Recipe)

from recipes.models import Measure
admin.site.register(Measure)

from recipes.models import FoodItem
admin.site.register(FoodItem)

from recipes.models import Ingredient
admin.site.register(Ingredient)

from recipes.models import Step
admin.site.register(Step)